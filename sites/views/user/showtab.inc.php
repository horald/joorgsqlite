<?php
$listarray = array ( array ( 'label' => 'Benutzer',
                             'name' => 'USER', 
                             'width' => 100, 
                             'type' => 'text',
                             'dbfield' => 'fldbez' ),
                     array ( 'label' => 'Username',
                             'name' => 'username', 
                             'width' => 100, 
                             'type' => 'text',
                             'dbfield' => 'fldusername' ),
                     array ( 'label' => 'Datei',
                             'name' => 'backgroundfilename', 
                             'width' => 100, 
                             'type' => 'text',
                             'fieldhide' => 'true',
                             'dbfield' => 'fldbackgroundfilename' ));

$pararray = array ( 'headline' => 'Benutzer',
                    'name' => 'USER', 
                    'dbtable' => 'tblbenutzer',
                    'orderby' => '',
                    'strwhere' => '',
                    'fldindex' => 'fldindex');
?>