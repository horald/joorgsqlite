<?php
$listarray = array ( array ( 'label' => 'Gewicht',
                             'name' => 'gewicht', 
                             'width' => 50, 
                             'type' => 'text',
                             'dbfield' => 'fldgewicht' ),
                     array ( 'label' => 'Uhrzeit',
                             'name' => 'uhrzeit', 
                             'width' => 50, 
                             'type' => 'time',
                             'default' => 'now()',
                             'dbfield' => 'flduhrzeit' ),
                     array ( 'label' => 'Datum',
                             'name' => 'datum', 
                             'width' => 90, 
                             'type' => 'date',
                             'default' => 'now()',
                             'dbfield' => 'flddatum' ),
                     array ( 'label' => 'Benutzer',
                             'name' => 'fltbenutzergewicht', 
                             'width' => 80, 
                             'type' => 'selectid',
                             'getdefault' => 'true',
                             'dbtable' => 'tblbenutzer',
                             'seldbfield' => 'fldbez',
                             'seldbindex' => 'fldindex',
                             'dbfield' => 'fldid_benutzer' ));

$filterarray = array ( array ( 'label' => 'Benutzer',
                             'name' => 'fltdiaetbenutzer', 
                             'width' => 10, 
                             'type' => 'selectid',
                             'sign' => '=',
                             'dbtable' => 'tblbenutzer',
                             'seldbfield' => 'fldbez',
                             'seldbindex' => 'fldindex',
                             'dbfield' => 'fldid_benutzer' ));
							 

$pararray = array ( 'headline' => 'Diät',
                    'dbtable' => 'tbldiaet',
                    'orderby' => 'flddatum,flduhrzeit',
                    'fldindex' => 'fldindex');
?>