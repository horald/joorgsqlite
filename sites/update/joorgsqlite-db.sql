-- Adminer 4.1.0 SQLite 3 dump

DROP TABLE IF EXISTS "android_metadata";
CREATE TABLE android_metadata (locale TEXT);


DROP TABLE IF EXISTS "sqlite_sequence";
CREATE TABLE sqlite_sequence(name,seq);


DROP TABLE IF EXISTS "tblEinkauf_liste";
CREATE TABLE "tblEinkauf_liste" (
  "fldIndex" integer NULL PRIMARY KEY AUTOINCREMENT,
  "fldReihenfolge" integer NULL DEFAULT '0',
  "fldBez" text NULL DEFAULT '0',
  "fldArtikelnr" text NULL DEFAULT '0',
  "fldTyp" text NULL DEFAULT '0',
  "fldSort" text NULL DEFAULT '0',
  "fldAbteilung" text NULL DEFAULT '0',
  "fldOrt" text NULL,
  "fldPreis" text NULL,
  "fldAnz" text NULL,
  "fldArchivDat" text NULL,
  "fldKonto" text NULL,
  "fldBarcode" text NULL,
  "flde01vorrat" integer NULL DEFAULT '0',
  "fldStatus" text NULL DEFAULT 'offen',
  "fldEinkaufDatum" text NULL,
  "fldEinkaufUhrzeit" text NULL,
  "fldid_kopf" integer NULL DEFAULT '0',
  "flddbsyncnr" integer NOT NULL DEFAULT '1',
  "fldtimestamp" text NULL,
  "flddbsyncstatus" text NULL DEFAULT 'SYNC'
);


DELIMITER ;;
CREATE TRIGGER "tblEinkauf_liste_au" AFTER   UPDATE ON "tblEinkauf_liste"
BEGIN
  update tblEinkauf_liste set fldtimestamp=DateTime('now','localtime') where fldIndex=new.fldIndex;
END;;
CREATE TRIGGER "tblEinkauf_liste_ai" AFTER INSERT ON "tblEinkauf_liste" FOR EACH ROW
BEGIN
  update tblEinkauf_liste set fldtimestamp=DateTime('now','localtime') where fldIndex=new.fldIndex;
END;;

DELIMITER ;

DROP TABLE IF EXISTS "tbladr_group";
CREATE TABLE "tbladr_group" (
  "fldindex" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "fldbez" text NOT NULL,
  "fldtyp" text NOT NULL
);


DROP TABLE IF EXISTS "tbladr_liste";
CREATE TABLE "tbladr_liste" (
  "fldindex" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "fldfirstname" text NOT NULL,
  "fldlastname" text NOT NULL,
  "fldemail" text NOT NULL
, "fldid_status" integer NULL, "flddbsyncstatus" text NULL);


DROP TABLE IF EXISTS "tbladr_lstgrp";
CREATE TABLE "tbladr_lstgrp" (
  "fldindex" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "fldid_liste" integer NOT NULL,
  "fldid_group" integer NOT NULL
);


DROP TABLE IF EXISTS "tblartikel";
CREATE TABLE "tblartikel" (
  "fldindex" integer NULL PRIMARY KEY AUTOINCREMENT,
  "fldBez" text NULL,
  "fldArtikelnr" text NULL,
  "fldTyp" text NULL,
  "fldSort" text NULL DEFAULT '',
  "fldAbteilung" text NULL,
  "fldOrt" text NULL,
  "fldPreis" text NULL,
  "fldKonto" text NULL,
  "fldAnz" text NULL,
  "fldJN" text NULL,
  "fldArchivDat" text NULL DEFAULT '',
  "fldbarcode" text NULL,
  "fldReihenfolge" integer NULL DEFAULT '999',
  "fldverpackbez" text NULL,
  "fldverpackmeng" text NULL,
  "fldtimestamp" text NULL,
  "flddbsyncnr" integer NULL DEFAULT '4',
  "flddbsyncstatus" numeric NULL DEFAULT 'SYNC'
, fldUpdateDat text NULL);


DELIMITER ;;
CREATE TRIGGER "tblartikel_ai" AFTER           INSERT ON "tblartikel"
BEGIN
  UPDATE tblartikel SET fldindex = new.fldindex + 9 WHERE fldindex = new.fldindex;
END;;
CREATE TRIGGER "tblartikel_au" AFTER      UPDATE ON "tblartikel"
BEGIN
  update tblartikel set fldtimestamp=DateTime('now','localtime') where fldindex=new.fldindex;
END;;

DELIMITER ;

DROP TABLE IF EXISTS "tblbenutzer";
CREATE TABLE "tblbenutzer" (
  "fldindex" integer NULL PRIMARY KEY AUTOINCREMENT,
  "fldbez" text NULL,
  "fldusername" text NULL,
  "fldbackgroundfilename" text NULL
, "flddbsyncstatus" text NULL DEFAULT 'SYNC');


DROP TABLE IF EXISTS "tblbilder";
CREATE TABLE "tblbilder" (
  "fldindex" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "fldfilename" text NOT NULL,
  "fldbez" text NOT NULL
);


DROP TABLE IF EXISTS "tblcalgrp";
CREATE TABLE "tblcalgrp" (
  "fldindex" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "fldbez" text NOT NULL
);


DROP TABLE IF EXISTS "tblerledigungen";
CREATE TABLE "tblerledigungen" (
  "fldIndex" integer NULL PRIMARY KEY AUTOINCREMENT,
  "fldprior" text NULL,
  "fldErledigDat" text NULL,
  "fldDatum" text NULL,
  "fldcategory" text NULL,
  "fldbez" text NULL,
  "fldid_erlgrp" integer NULL DEFAULT '0',
  "fldstatus" text NULL,
  "flddbsyncstatus" numeric NULL DEFAULT 'SYNC'
);


DROP TABLE IF EXISTS "tbletagen";
CREATE TABLE tbletagen (fldproz TEXT, fldindex INTEGER PRIMARY KEY, fldbez TEXT);


DROP TABLE IF EXISTS "tblfahrtenbuch";
CREATE TABLE "tblfahrtenbuch" (
  "fldIndex" integer NULL PRIMARY KEY AUTOINCREMENT,
  "fldFahrzeug" text NULL,
  "fldVondatum" text NULL,
  "fldBisdatum" text NULL DEFAULT '1970-01-01',
  "fldVonkm" text NULL,
  "fldBiskm" text NULL,
  "fldDauer" text NULL,
  "fldZeittarif" text NULL,
  "fldStatus" text NULL,
  "fldind_datum" text NULL DEFAULT '0',
  "fldid_adr" numeric NULL DEFAULT '0',
  "fldKmpreis" text NULL,
  "fldtimestamp" text NULL,
  "flddbsyncnr" integer NULL DEFAULT '8',
  "flddbsyncstatus" text NULL DEFAULT 'SYNC'
);


DROP TABLE IF EXISTS "tblfilemanager";
CREATE TABLE "tblfilemanager" (
  "fldindex" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "fldBez" text NULL,
  "fldPfad" text NULL,
  "fldWildcard" text NULL,
  "fldid_fmgrp" integer NULL,
  "flddbsyncstatus" text NOT NULL DEFAULT 'SYNC'
);


DROP TABLE IF EXISTS "tblfilter";
CREATE TABLE "tblfilter" (
  "fldtablename" text NULL,
  "fldwert" text NULL,
  "fldfeld" text NULL,
  "fldindex" integer NULL PRIMARY KEY AUTOINCREMENT,
  "fldmaske" text NULL,
  "fldName" text NULL
);


DROP TABLE IF EXISTS "tblfmgrp";
CREATE TABLE "tblfmgrp" (
  "fldindex" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "fldbez" text NOT NULL
);


DROP TABLE IF EXISTS "tblfunc";
CREATE TABLE "tblfunc" (
  "fldindex" integer NULL PRIMARY KEY AUTOINCREMENT,
  "fldBez" text NULL,
  "fldphp" text NULL,
  "fldMenuID" text NULL,
  "fldTyp" text NULL,
  "fldTarget" text NULL,
  "fldParam" text NULL,
  "fldAktiv" text NULL,
  "fldName" text NULL,
  "fldlanguage" text NULL,
  "fldtimestamp" text NULL,
  "fldversion" text NULL,
  "flddbsyncstatus" text NULL DEFAULT 'SYNC'
);


DROP TABLE IF EXISTS "tblgepaeck";
CREATE TABLE "tblgepaeck" (
  "fldindex" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "fldbez" text NOT NULL
);


DROP TABLE IF EXISTS "tblgooglecal";
CREATE TABLE "tblgooglecal" (
  "fldindex" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "fldbez" text NULL,
  "fldgruppe" text NULL,
  "fldcal" text NULL,
  "fldcolor" text NULL,
  "fldjn" text NOT NULL DEFAULT 'J',
  "flddbsyncnr" integer NOT NULL DEFAULT '8',
  "fldtimestamp" text NOT NULL,
  "flddbsyncstatus" text NOT NULL DEFAULT 'SYNC'
);


DROP TABLE IF EXISTS "tblgrperl";
CREATE TABLE `tblgrperl` (
  `fldindex` bigint(20),
  `fldbez` varchar(200)
);


DROP TABLE IF EXISTS "tblhelppage";
CREATE TABLE "tblhelppage" (
  "fldindex" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "fldpageno" integer NOT NULL,
  "fldpagename" text NOT NULL
, "fldhelpurl" text NULL, "fldheadline" text NULL, "flddbsyncstatus" text NULL DEFAULT 'SYNC');


DROP TABLE IF EXISTS "tblhelprows";
CREATE TABLE "tblhelprows" (
  "fldindex" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "fldid_helppage" integer NOT NULL,
  "fldrow" integer NOT NULL,
  "fldcontent" text NOT NULL
);


DROP TABLE IF EXISTS "tblindex";
CREATE TABLE "tblindex" (
  "fldindex" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "fldtable" text NOT NULL,
  "fldid" integer NOT NULL
);


DROP TABLE IF EXISTS "tblkategorie";
CREATE TABLE tblkategorie (fldindex INTEGER PRIMARY KEY, fldbez TEXT);


DROP TABLE IF EXISTS "tblktoinhaber";
CREATE TABLE "tblktoinhaber" (
  "fldindex" integer NULL PRIMARY KEY AUTOINCREMENT,
  "fldBez" text NULL
);


DROP TABLE IF EXISTS "tblktokonten";
CREATE TABLE tblktokonten (fldindex INTEGER PRIMARY KEY AUTOINCREMENT, fldKurz TEXT, fldBez TEXT, fldTyp TEXT, flddbsyncstatus txt DEFAULT 'SYNC');


DROP TABLE IF EXISTS "tblktosal";
CREATE TABLE 'tblktosal' (
  `fldindex` INTEGER PRIMARY KEY AUTOINCREMENT,
  `fldDatum` text,
  `fldUhrzeit` text,
  `fldPos` int,
  `fldBez` text,
  `fldArt` text,
  `fldKonto` text,
  `fldBetrag` text,
  `fldInhaber` text,
  `fldKtoart` text,'fldFix' TEXT,'fldorgdatum' TEXT,
  `fldfremdbetrag` text,
  `fldwaehrung` text,
  `fldDetailind` int NOT NULL DEFAULT '0',
  `fldUmbuchinhaber` text,
  `fldsel` text NOT NULL DEFAULT 'N',
  `fldid_ort` int,
  `fldtyp` text,
  `fldfilename` text,
  `fldcomputer` text,
  `fldtimestamp` text,
  `flddbsyncnr` int NOT NULL DEFAULT '1',
  `flddbsyncstatus` text NOT NULL DEFAULT 'SYNC',
  `flddel` text NOT NULL DEFAULT 'N'
);


DROP TABLE IF EXISTS "tblktosal_alt";
CREATE TABLE "tblktosal_alt" (
  "fldindex" integer NULL PRIMARY KEY AUTOINCREMENT,
  "fldDatum" text NULL,
  "fldUhrzeit" text NULL,
  "fldPos" integer NULL,
  "fldBez" text NULL,
  "fldArt" text NULL,
  "fldKonto" text NULL,
  "fldBetrag" text NULL,
  "fldInhaber" text NULL,
  "fldKtoart" text NULL,
  "fldFix" numeric NULL,
  "fldorgdatum" text NULL,
  "fldfremdbetrag" text NULL,
  "fldtyp" text NULL,
  "flddel" text NULL DEFAULT 'N',
  "fldtimestamp" text NULL,
  "flddbsyncnr" integer NULL DEFAULT '1',
  "flddbsyncstatus" integer NULL DEFAULT 'SYNC'
, fldwaehrung text);


DROP TABLE IF EXISTS "tblktostamm";
CREATE TABLE tblktostamm (fldindex INTEGER PRIMARY KEY, fldDatum TEXT, fldUhrzeit TEXT, fldBez TEXT, fldKonto TEXT, fldInhaber TEXT, fldBetrag TEXT, fldFix NUMERIC, flddbsyncstatus default 'SYNC');


DROP TABLE IF EXISTS "tblktostamm_zuord";
CREATE TABLE "tblktostamm_zuord" (
  "fldindex" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "fldid_ktostamm" integer NOT NULL,
  "fldid_ktoinhaber" integer NOT NULL
);


DROP TABLE IF EXISTS "tblktotyp";
CREATE TABLE tblktotyp (fldtyp TEXT, fldindex INTEGER PRIMARY KEY, fldbez TEXT);


DROP TABLE IF EXISTS "tbllektion";
CREATE TABLE "tbllektion" (
  "fldindex" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "fldbez" text NOT NULL,
  "fldtimestamp" text NULL,
  "flddbsyncnr" integer NULL DEFAULT '4',
  "flddbsyncstatus" integer NULL DEFAULT 'SYNC'
);


DELIMITER ;;
CREATE TRIGGER "tbllektion_au" AFTER UPDATE ON "tbllektion" FOR EACH ROW
BEGIN
  update tbllektion set fldtimestamp=DateTime('now','localtime') where fldIndex=new.fldIndex;
END;;

DELIMITER ;

DROP TABLE IF EXISTS "tblmenu_grp";
CREATE TABLE "tblmenu_grp" (
  "fldindex" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "fldbez" text NOT NULL
);


DROP TABLE IF EXISTS "tblmenu_liste";
CREATE TABLE "tblmenu_liste" (
  "fldid_parent" numeric NULL,
  "fldglyphicon" text NULL,
  "fldlink" text NULL,
  "fldsort" text NULL,
  "fldbez" text NULL,
  "fldindex" integer NULL PRIMARY KEY AUTOINCREMENT,
  "fldmenu" text NULL,
  "fldview" text NULL,
  "fldlanguage" text NULL,
  "flddbsyncstatus" text NULL,
  "fldparam" text NULL,
  "fldusername" text NULL,
  "fldusermenu" text NULL DEFAULT 'N'
);


DROP TABLE IF EXISTS "tblmenu_zuord";
CREATE TABLE "tblmenu_zuord" (
  "fldindex" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "fldid_menu" integer NOT NULL,
  "fldid_menugrp" integer NOT NULL
);


DROP TABLE IF EXISTS "tblnotiz";
CREATE TABLE "tblnotiz" (
  "fldindex" integer NULL,
  "flddatum" numeric NULL,
  "flderldatum" numeric NULL DEFAULT '1900-01-01',
  "fldnr" text NULL,
  "fldbez" text NULL,
  "fldbemerk" text NULL,
  "fldarchivdat" numeric NULL DEFAULT '1900-01-01',
  "fldstatus" text NULL DEFAULT '''''',
  "fldart" text NULL DEFAULT '''''',
  "fldbenutzer" text NULL DEFAULT '''''',
  "fldid_gruppe" integer NULL,
  "fldid_themen" integer NULL,
  "fldlink" text NULL,
  "fldTarget" text NULL,
  "fldfilename" text NULL,
  "fldid_status" integer NULL,
  "flddbsyncnr" integer NULL DEFAULT '4',
  "fldtimestamp" numeric NULL DEFAULT 'CURRENT_TIMESTAMP',
  "flddbsyncstatus" text NULL DEFAULT 'SYNC'
);


DROP TABLE IF EXISTS "tblorte";
CREATE TABLE "tblorte" (
  "fldkurz" text NULL,
  "fldid_moebel" numeric NULL,
  "fldproz" numeric NULL,
  "fldid_etagealt" numeric NULL,
  "fldhoehe" numeric NULL,
  "fldbreite" numeric NULL,
  "fldykoor" numeric NULL,
  "fldxkoor" numeric NULL,
  "fldview" text NULL,
  "fldid_zimmer" numeric NULL,
  "fldindex" integer NULL PRIMARY KEY AUTOINCREMENT,
  "fldBez" text NULL,
  "fldo01typ" text NULL,
  "fldid_etage" numeric NULL
, "fldid_suchobj" numeric NULL);


DROP TABLE IF EXISTS "tblortkurz";
CREATE TABLE "tblortkurz" (
  "fldindex" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "fldkurz" text NULL,
  "fldbez" text NULL,
  "fldid_zimmer" integer NULL DEFAULT '0',
  "fldid_moebel" integer NULL DEFAULT '0',
  "fldid_fach" integer NULL DEFAULT '0',
  "flddbsyncstatus" text NULL DEFAULT 'SYNC',
  "flddbsyncnr" integer NULL DEFAULT '8',
  "fldtimestamp" text NULL
);


DROP TABLE IF EXISTS "tblpreisentw";
CREATE TABLE "tblpreisentw" (
  "fldindex" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "fldBez" text NOT NULL,
  "fldOrt" text NOT NULL,
  "fldPreis" text NOT NULL,
  "fldDatum" text NOT NULL,
  "flddbsyncstatus" text NOT NULL DEFAULT 'SYNC'
);


DROP TABLE IF EXISTS "tblprior";
CREATE TABLE tblprior (fldindex INTEGER PRIMARY KEY AUTOINCREMENT, fldprior TEXT);


DROP TABLE IF EXISTS "tblrechdat";
CREATE TABLE "tblrechdat" (
  "fldindex" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "flddatum" text NOT NULL
);


DROP TABLE IF EXISTS "tblrefsuchobj";
CREATE TABLE tblrefsuchobj (fldid_zimmer NUMERIC, fldid_moebel NUMERIC, fldtyp TEXT, fldid_orte NUMERIC, fldproz NUMERIC, fldindex INTEGER PRIMARY KEY, fldid_suchobj NUMERIC, fldid_etage NUMERIC);


DROP TABLE IF EXISTS "tblsel";
CREATE TABLE "tblsel" (
  "fldindex" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "fldbez" text NOT NULL
);


DROP TABLE IF EXISTS "tblsession";
CREATE TABLE "tblsession" (
  "fldindex" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "fldid" integer NOT NULL
);


DROP TABLE IF EXISTS "tblsprache";
CREATE TABLE "tblsprache" (
  "fldindex" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "fldbez" text NOT NULL,
  "fldlanguage" text NOT NULL,
  "fldtimestamp" text NULL,
  "flddbsyncnr" text NOT NULL DEFAULT '4',
  "flddbsyncstatus" text NOT NULL DEFAULT 'SYNC'
);


DELIMITER ;;
CREATE TRIGGER "tblsprache_au" AFTER UPDATE ON "tblsprache" FOR EACH ROW
BEGIN
   update tblsprache set fldtimestamp=DateTime('now','localtime') where fldIndex=new.fldIndex;
END;;

DELIMITER ;

DROP TABLE IF EXISTS "tblstatus";
CREATE TABLE tblstatus (fldtyp TEXT, fldindex INTEGER PRIMARY KEY, fldbez TEXT);


DROP TABLE IF EXISTS "tblstdplan";
CREATE TABLE `tblstdplan` (
  `fldindex` integer NOT NULL,
  `fldid_std` integer NOT NULL,
  `fldid_wotag` integer NOT NULL,
  `fldbez` text NOT NULL,
  `fldid_user` integer NOT NULL,
  `fldnr` text NOT NULL,
  `flddbsyncstatus` text NOT NULL DEFAULT 'SYNC',
  PRIMARY KEY (`fldindex`)
);


DROP TABLE IF EXISTS "tblstdwotag";
CREATE TABLE "tblstdwotag" (
  "fldindex" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "fldspalte" text NOT NULL,
  "fldbez" text NOT NULL,
  "fldfarbe" text NOT NULL
);


DROP TABLE IF EXISTS "tblstdzeit";
CREATE TABLE `tblstdzeit` (
  `fldindex` integer NOT NULL,
  `fldbez` text NOT NULL,
  PRIMARY KEY (`fldindex`)
);


DROP TABLE IF EXISTS "tblsuchobj";
CREATE TABLE tblsuchobj (fldid_user NUMERIC, fldindex INTEGER PRIMARY KEY, fldbez TEXT);


DROP TABLE IF EXISTS "tblsuchtyp";
CREATE TABLE "tblsuchtyp" (
  "fldindex" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "fldbez" text NOT NULL
, "fldsort" text NULL);


DROP TABLE IF EXISTS "tblsyncstatus";
CREATE TABLE "tblsyncstatus" (
  "fldindex" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "fldtable" text NOT NULL,
  "fldtimestamp" text NOT NULL
);


DROP TABLE IF EXISTS "tbltable";
CREATE TABLE "tbltable" (
  "fldindex" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "fldbez" integer NOT NULL,
  "fldtyp" integer NOT NULL
);


DROP TABLE IF EXISTS "tbltc_reiseliste";
CREATE TABLE "tbltc_reiseliste" (
  "fldindex" integer NULL PRIMARY KEY AUTOINCREMENT,
  "fldbez" text NOT NULL,
  "fldgepaeck" text NULL,
  "fldreisegrp" text NULL,
  "fldstatus" text NULL,
  "fldid_benutzer" integer NULL,
  "fldid_gepaeck" integer NULL,
  "fldsel" text NULL,
  "fldAnz" integer NULL,
  "fldgewicht" text NULL,
  "flddbsyncstatus" text NOT NULL DEFAULT '''SYNC'
);


DROP TABLE IF EXISTS "tbltermin_lst";
CREATE TABLE "tbltermin_lst" (
  "fldindex" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "fldbez" text NOT NULL,
  "fldvondatum" text NOT NULL,
  "fldserientyp" text NOT NULL,
  "fldserientage" integer NOT NULL
);


DROP TABLE IF EXISTS "tblversion";
CREATE TABLE tblversion (fldindex INTEGER PRIMARY KEY, fldbez TEXT, fldversion TEXT, flddatum TEXT);


DROP TABLE IF EXISTS "tblvokabeln";
CREATE TABLE "tblvokabeln" (
  "fldindex" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "fldsprache_eins" text NOT NULL,
  "fldsprache_zwei" text NOT NULL,
  "fldfremdsprache" text NULL,
  "fldlektion" text NOT NULL,
  "fldanzgelernt" integer NOT NULL DEFAULT '0',
  "fldanzfehler" integer NOT NULL DEFAULT '0',
  "fldtimestamp" text NULL,
  "flddbsyncnr" integer NOT NULL DEFAULT '4',
  "flddbsyncstatus" text NOT NULL DEFAULT 'SYNC'
);


DELIMITER ;;
CREATE TRIGGER "tblvokabeln_au" AFTER UPDATE ON "tblvokabeln" FOR EACH ROW
BEGIN
  update tblvokabeln set fldtimestamp=DateTime('now','localtime') where fldIndex=new.fldIndex;
END;;

DELIMITER ;

DROP TABLE IF EXISTS "tblvorrat";
CREATE TABLE "tblvorrat" (
  "fldindex" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "fldbarcode" text NULL,
  "fldanz" text NULL,
  "fldortkurz" text NULL,
  "fldmhdatum" text NULL,
  "fldtimestamp" text NULL,
  "flddbsyncnr" integer NULL,
  "flddbsyncstatus" text NULL DEFAULT 'SYNC'
);


-- 
