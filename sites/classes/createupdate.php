<?php
header("content-type: text/html; charset=utf-8");
include("bootstrapfunc.php");
bootstraphead("");
bootstrapbegin("Update erzeugen","");
chdir('../');
$file="updatelist.txt";
$output = shell_exec("git status -s");
//echo "<pre>$output</pre>";
//$r = file_put_contents("../".$file, $output);
$r = file_put_contents($file, $output);
if ($r) {
  echo "<div class='alert alert-success'>";
  echo "Speichern von ".$file." erfolgreich.<br>";	
  echo "</div>"; 

  $string = file_get_contents("version.json");
  $json_a = json_decode($string, true);
  $versnr=$json_a['versnr'];
  $zip = new ZipArchive();
  $filename = "sites/update/Update".$versnr.".zip";
  if ($zip->open($filename, ZipArchive::CREATE)!==TRUE) {
    exit("cannot open <$filename>\n");
  }
  $fh = fopen($file,'r');
  echo "<pre>";
  while ($line = fgets($fh)) {
  	 $line=rtrim($line);
  	 $endstr = substr($line, -1);
  	 $weiter=TRUE;
  	 if ($endstr=="~") {
  	 	$weiter=FALSE;
  	 }
  	 $endstr = substr($line, -10);
  	 if ($endstr=="config.php") {
  	 	$weiter=FALSE;
  	 }
  	 $endstr = substr($line, -6);
  	 if ($endstr=="git.sh") {
  	 	$weiter=FALSE;
  	 }
  	 $endstr = substr($line, -14);
  	 if ($endstr=="updatelist.txt") {
  	 	$weiter=FALSE;
  	 }
  	 $endstr = substr($line, -14);
  	 if ($endstr=="joorgsqlite.db") {
  	 	$weiter=FALSE;
  	 }
  	 $endstr = substr($line, -4);
  	 if ($endstr==".zip") {
  	 	$weiter=FALSE;
  	 }
  	 if ($weiter) {
  	 	$line=substr($line,3);
      $zip->addFile($line);
      echo $line."<br>";
    }
  }
  echo "numfiles: " . $zip->numFiles . "<br>";
  echo "status:" . $zip->status . "<br>";
  echo "</pre>";
  fclose($fh);
  $zip->close();  
  echo "<div class='alert alert-info'>";
  echo "Zip-Datei Update".$versnr.".zip erstellt.<br>";	
  echo "</div>"; 
 
} else {
  echo "<div class='alert alert-warning'>";
  echo "Speichern von ".$file." fehlgeschlagen! (".$r.")<br>";	
  echo "</div>"; 
}
$menuid=$_GET['id'];
chdir('classes');
echo "<a href='../index.php?id=".$menuid."' class='btn btn-primary btn-sm active' role='button'>Zurück</a>";
bootstrapend();
?>