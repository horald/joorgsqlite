if [ "$1" != "" ]; then
  case "$1" in
    origin)
      git add --all
      if [ "$2" != "" ]; then
        git commit -m "$2"
      else
        version=$(awk -F ":" '/versnr/ {print $2}' version.json)
        version=${version:1:5}
        git commit -m "undocumented Changes of Version $version"
      fi  
      echo "======================"
      echo "Hochladen mit:"
      echo "git push origin master"
      echo "======================"
      ;;
    showversion)
      version=$(awk -F ":" '/versnr/ {print $2}' version.json)
      version=${version:1:5}
      versdat=$(awk -F ":" '/versdat/ {print $2}' version.json)
      versdat=${versdat:1:10}
      echo "Aktuelle Version $version vom $versdat"
      ;;  
    upversion)
      version=$(awk -F ":" '/versnr/ {print $2}' version.json)
      version=${version:1:5}
      echo "=== old ==="
      cat version.json
      versvrn='{"versnr":"'
      vershtn='",'
      versdatvrn=' "versdat":"'
      DATE=`date +%d.%m.%Y`
      versdathtn='"}'
      echo $versvrn $version $vershtn | awk '{print $1$2 + 0.001$3}' > version.json
      echo "$versdatvrn$DATE$versdathtn" >> version.json
      echo "=== create ==="
      cat version.json
      version=$(awk -F ":" '/versnr/ {print $2}' version.json)
      version=${version:1:5}
      git tag -a $version -m "Version "$version
      echo "=== new ==="
      echo "$version"
      ;;           
  esac
else
  echo "Bitte Übergabeparameter angeben: origin, showversion, upversion"
fi
