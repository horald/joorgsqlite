CREATE TABLE `tblstdplan` (
  `fldindex` integer NOT NULL,
  `fldid_std` integer NOT NULL,
  `fldid_wotag` integer NOT NULL,
  `fldbez` text NOT NULL,
  `fldid_user` integer NOT NULL,
  `fldnr` text NOT NULL,
  `flddbsyncstatus` text NOT NULL DEFAULT 'SYNC',
  PRIMARY KEY (`fldindex`)
);