<?php
/************************
* Author: Horst Meyer
* date..: 2020-08-19
* remark: start modul for Value added tax conversion
************************/
include("../sites/mwstumrech.ini");
include("mwstumrechfunc.php");
include("bootstrapfunc.php");
$idwert=$_GET['id'];
if (isset($_GET['preisA'])) {
  $preisA=$_GET['preisA'];	
} else {
  $preisA=0;
}
if (isset($_GET['preisB'])) {
  $preisB=$_GET['preisB'];	
} else {
  $preisB=0;
}
bootstraphead("");
bootstrapbegin("MwSt-Umrechnung","");
$mwstumrech = $_GET['mwstumrech'];
If ($mwstumrech==1) {
  $mwsttypA=$_POST['mwsttypA'];
  $preisA=$_POST['preisA'];
  $mwsttypB=$_POST['mwsttypB'];
  $preisB=$_POST['preisB'];
  mwstumrechnung($idwert,$mwsttypA,$preisA,$mwsttypB,$preisB,$vonmwstsatz1,$nchmwstsatz1,$vonmwstsatz2,$nchmwstsatz2,$waehrung);
} else {
  mwstumrech_abfrage($idwert,$preisA,$preisB,$vonmwstsatz1,$nchmwstsatz1,$vonmwstsatz2,$nchmwstsatz2);
}
echo "<a href='../index.php?id=".$idwert."' class='btn btn-primary btn-sm active' role='button'>Zurück</a>"; 
bootstrapend();
?>