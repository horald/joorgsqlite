<?php
include("bootstrapfunc.php");
include("../config.php");
bootstraphead("");
bootstrapbegin("Joorgportal - Get Version<br>","");
if ($proxy!="") {
  $aContext = array(
    'http' => array(
        'proxy'           => $proxy,
        'request_fulluri' => true,
    ),
  );
  $cxContext = stream_context_create($aContext);
  $string = file_get_contents("https://gitlab.com/horald/joorgsqlite/-/raw/master/version.json", False, $cxContext);  
} else {  
  $string = file_get_contents("https://gitlab.com/horald/joorgsqlite/-/raw/master/version.json");
}  
$json_a = json_decode($string, true);
$versnr=$json_a['versnr'];
$versdat=$json_a['versdat'];
$callback=urldecode($_GET['callback']);
$servername=$_SERVER['SERVER_NAME'];
$serverport=$_SERVER['SERVER_PORT'];
if ($servername==="::") {
  $servername="localhost";	
}
echo "<table class='table table-hover'>";
echo "<tr><td>Version:</td><td>".$versnr."</td></tr>";
echo "<tr><td>Vers.-Dat.:</td><td>".$versdat."</td></tr>";
echo "<tr><td>Servername:</td><td>".$servername."</td></tr>";
echo "<tr><td>Serverport:</td><td>".$serverport."</td></tr>";
echo "</table>";
if ($serverport!=="80") {
  $callback=str_replace($servername,$servername.":".$serverport,$callback);	
}	
echo "<div class='alert alert-info'>";
echo "Callback..:".$callback."<br>";
echo "</div>";
echo "<a href='".$callback."?versnr=".$versnr."&versdat=".$versdat."' class='btn btn-primary btn-sm active' role='button'>Back</a>";
bootstrapend();
echo "<meta http-equiv='refresh' content='0; URL=".$callback."?versnr=".$versnr."&versdat=".$versdat."'>";

?>