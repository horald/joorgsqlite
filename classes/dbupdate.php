<?php
include("bootstrapfunc.php");
bootstraphead("");
bootstrapbegin("Datenbankupdate","");
$dbupdatefile=$_POST['dbupdatefile'];
echo "<div class='alert alert-info'>";
echo $dbupdatefile." wird installiert...<br>";
echo "</div>";
$pFilename="../".$dbupdatefile;
$handfile = fopen($pFilename, "r");
$db = new SQLite3('../data/joorgsqlite.db');
while (($line = fgets($handfile)) !== false) {
  $db->exec($line);
  echo "<div class='alert alert-info'>";
  echo $line;
  echo "</div>";
}
$db->close();
fclose ($handfile); 
unlink($pFilename);
if (!file_exists($pFilename)) {
  echo "<div class='alert alert-success'>";
  echo "DBUpdate-Datei wurde gelöscht...<br>";
  echo "</div>";
} else {
  echo "<div class='alert alert-danger'>";
  echo "Löschen der DBUpdate-Datei fehlgeschlagen! Bitte händisch löschen.";
  echo "</div>";
}

echo "<a href='../index.php' class='btn btn-primary btn-sm active' role='button'>Joorgportal</a>"; 
bootstrapend();
?>