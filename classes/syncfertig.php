<?php
include("bootstrapfunc.php");
include("dbtool.php");
bootstraphead('');
bootstrapbegin("Datenaustausch",'');
$menu=$_GET['menu'];
$idwert=$_GET['idwert'];
$dbtyp=$_POST['dbtyp'];
$database=$_POST['database'];
$dbuser=$_POST['dbuser'];
$dbpassword=$_POST['dbpassword'];
$dbsyncnr=$_POST['dbsyncnr'];
$dbtable=$_POST['dbtable'];
$date = date('Y-m-d');
$time = date('H:i:s', time());
$timestamp=$date." ".$time;
$dbnchopen=dbopentyp($dbtyp,$database,$dbuser,$dbpassword);
$query="SELECT * FROM tblsyncstatus WHERE fldtable='".$dbtable."' AND flddbsyncnr=".$dbsyncnr;
$resst = dbquerytyp($dbtyp,$dbnchopen,$query);
if ($linst = dbfetchtyp($dbtyp,$resst)) {
  $qry="UPDATE tblsyncstatus SET fldtable='".$dbtable."', fldtimestamp='".$timestamp."', flddbsyncnr=".$dbsyncnr;
} else {
  $qry="INSERT INTO tblsyncstatus (fldtable,fldtimestamp,flddbsyncnr) VALUES ('".$dbtable."','".$timestamp."',".$dbsyncnr.")";
}
//echo $qry."<br>";
dbexecutetyp($dbtyp,$dbnchopen,$qry);
$callbackurl=$_POST['callbackurl'];
echo "<a href='".$callbackurl."'  class='btn btn-primary btn-sm active' role='button'>Zurück</a><br> ";
echo "<div class='alert alert-info'>";
echo "Timestamp:".$timestamp."<br>";
echo "Datensynchronisation abgeschlossen.";
echo "</div>";
bootstrapend();
?>