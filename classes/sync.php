<?php
include("../config.php");
include("bootstrapfunc.php");
include("dbtool.php");
include("syncfunc.php");
$menu=$_GET['menu'];
$idwert=$_GET['idwert'];
include("../sites/views/".$viewprefix.$menu."/showtab.inc.php");
$auto="N";
bootstraphead('');
bootstrapbegin("Datenaustausch",'');
echo "<a href='showtab.php?menu=".$menu."&idwert=".$idwert."'  class='btn btn-primary btn-sm active' role='button'>Zurück</a> ";
$status=$_POST['status'];
switch ( $status ) {
  case 'sync':
    $typ=$_POST['typ'];
    $dbase=$_POST['datenbank'];
    $dbtable=$pararray['dbtable'];
    $dbfldindex=$pararray['fldindex'];
    $debug=$_POST['debug'];
    if ($debug) {
    	$debug="J";
    } else {
    	$debug="N";
    }
    $utf8encode=$_POST['utf8encode'];
    if ($utf8encode) {
    	$utf8encode="J";
    } else {
    	$utf8encode="N";
    }
    if ($typ=="local") { 	
      auslesen();
    } else {
      fernabfrage($gdbtyp,$gdbname,$gdbuser,$gdbpass,$menu,$idwert,$dbase,$dbtable,$debug,$utf8encode,$dbfldindex);
    }
  break;
  case 'einspielen':
    $anzds=$_POST['stranzds'];
    einlesen($gdbtyp,$gdbname,$gdbuser,$gdbpass,$menu,$idwert,$anzds);
  break;
  default:
    showauswahl($gdbtyp,$gdbname,$gdbuser,$gdbpass,$menu,$idwert,$auto);
}
bootstrapend();
?>