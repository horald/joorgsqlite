<?php
header("content-type: text/html; charset=utf-8");
include("bootstrapfunc.php");
bootstraphead("");
bootstrapbegin("Update einspielen","");
$versnr=$_POST['versnr'];
echo "<div class='alert alert-info'>";
echo "Version ".$versnr." updaten.";
echo "</div>";

chdir('../');
$file="Update".$versnr.".zip";
$path="/var/www/html/own/joorgsqlite/";
$filename=$path.$file;
if (file_exists($filename)) {
  //$path = pathinfo(realpath($file), PATHINFO_DIRNAME);
  //$len=strlen($path);
  //$path = substr($path,0,$len-12);
  $zip = new ZipArchive;
  if ($zip->open($file) === TRUE) {
      $zip->extractTo($path);
      $zip->close();
      echo "<div class='alert alert-success'>";
      echo "Datei $file wurde erfolgreich nach $path entpackt.";
      echo "</div>";
  } else {
      echo "<div class='alert alert-warning'>";
      echo 'Zip-Fehler aufgetreten!<br>';
      echo "</div>";
  }
  echo "<div class='alert alert-warning'>";
  echo "Update eingespielt.<br>";
  echo "</div>";
} else {
    echo "<div class='alert alert-danger'>";
    echo "Die Datei $filename existiert nicht";
    echo "</div>";
}
chdir('classes');

echo "<a href='about.php' class='btn btn-primary btn-sm active' role='button'>Zurück</a>";
bootstrapend();
?>