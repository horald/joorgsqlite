<?php

function showauswahl($gdbtyp,$gdbname,$gdbuser,$gdbpass,$menu,$idwert,$auto) {
  $value="Sync starten";
  echo "<br>";
  echo "<form class='form-horizontal' method='post' action='sync.php?showsync=1&menu=".$menu."&idwert=".$idwert."'>";
  
  $dbopen=dbopentyp($gdbtyp,$gdbname,$gdbuser,$gdbpass);
  $fquery = "SELECT * FROM tbldatabase ";
  $fresult = dbquerytyp($gdbtyp,$dbopen,$fquery);
  echo "<div class='control-group'>";
  echo "  <label class='control-label' style='text-align:left' for='input01'>Datenbank:</label>";
  echo "  <select name='datenbank' size='1'>";
  while ($fline = dbfetchtyp($gdbtyp,$fresult)) {
  	 if ($fline['fldaktiv']=="J") {
      echo "<option style='background-color:#c0c0c0;' value='".$fline['fldindex']."' selected>".$fline['fldbemerk']."</option>";
  	 } else {
      echo "<option style='background-color:#c0c0c0;' value='".$fline['fldindex']."' >".$fline['fldbemerk']."</option>";
    }
  }
  echo "  </select>";
  echo "</div>";
  
  
  echo "<div class='control-group'>";
  echo "  <label class='control-label' style='text-align:left' for='input01'>Direktion:</label>";
  echo "<select name='typ' size='1'>";
  echo "<option style='background-color:#c0c0c0;' >local</option>";
  echo "<option style='background-color:#c0c0c0;' selected>remote</option>";
  echo "</select>";
  echo "</div>";
  echo "          <div class='control-group'>";
  echo "              <input type='checkbox' name='debug'> Zeig Fehlermeldung<br>";
  echo "              <input type='checkbox' name='utf8encode'> utf8 encode<br>";
  echo "              <input type='checkbox' name='auto'> Auto-Sync<br>";
  echo "          </div>";
  echo "<br>";
  echo "<input type='hidden' name='status' value='sync' />";
  if ($auto=="J") {
    echo "<input type='hidden' name='auto' value=".$auto." />";
    echo "<input type='submit' name='submit' id='btnsubmit' value='Submit' />";
  } else {
    echo "<dd><input type='submit' value='".$value."' /></dd>";
  }
  echo "</form>";
}

function auslesen() {
  echo "auslesen noch nicht fertig!<br>";
}

function fernabfrage($gdbtyp,$gdbname,$gdbuser,$gdbpass,$menu,$idwert,$dbase,$dbtable,$debug,$utf8encode,$dbfldindex) {
  $aktpfad=$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];  
  $callbackurl="http://".$aktpfad."?menu=".$menu."&idwert=".$idwert;
  $auto=$_POST['auto'];
  //echo $auto."=auto<br>";

  $dbnchopen=dbopentyp($gdbtyp,$gdbname,$gdbuser,$gdbpass);
  $dbcolumn=getdbcolumn($gdbtyp,$gdbname,$dbtable,$gdbuser,$gdbpass);
  //echo $dbcolumn."=dbcolumn<br>";
  
  $dbopen=dbopentyp($gdbtyp,$gdbname,$gdbuser,$gdbpass);
  $fquery = "SELECT * FROM tbldatabase WHERE fldindex=".$dbase;
  $fresult = dbquerytyp($gdbtyp,$dbopen,$fquery);
  $fline = dbfetchtyp($gdbtyp,$fresult);
  $website=$fline['fldpfad']."classes/syncauslesen.php?menu=".$menu."&idwert=".$idwert."&debug=".$debug;
  echo "<div class='alert alert-info'>";
  if ($debug="J") {
  	 echo "website=".$website;
  	 echo "<br>";
  }
  echo "Remote-Abfrage<br>"; 
  echo $fline['fldbemerk']." auslesen.";
  echo "</div>";
  echo "<form class='form-horizontal' method='post' action='".$website."'>";
  echo "<input type='hidden' name='callbackurl' value='".$callbackurl."' />";
  echo "<input type='hidden' name='remotepfad' value='".$fline['fldpfad']."' />";
  echo "<input type='hidden' name='utf8encode' value='".$utf8encode."' />";
  echo "<input type='hidden' name='dbtable' value='".$dbtable."' />";
  echo "<input type='hidden' name='dbfldindex' value='".$dbfldindex."' />";
  echo "<input type='hidden' name='dbtyp' value='".$fline['flddbtyp']."' />";
  echo "<input type='hidden' name='dbcolumn' value='".json_encode($dbcolumn)."' />";
  echo "<input type='hidden' name='database' value='".$fline['flddbbez']."' />";
  echo "<input type='hidden' name='dbuser' value='".$fline['flddbuser']."' />";
  echo "<input type='hidden' name='dbpassword' value='".$fline['flddbpassword']."' />";
  echo "<input type='hidden' name='dbsyncnr' value='".$fline['flddbsyncnr']."' />";


  if ($auto=="J") {
    echo "<input type='hidden' name='auto' value=".$auto." />";
    echo "<input type='submit' name='submit' id='btnsubmit' value='Submit' />";
  } else {
    echo "<input type='submit' value='Daten einspielen' /><br>";
  }
  echo "</form>";
}

function einlesen($gdbtyp,$gdbname,$gdbuser,$gdbpass,$menu,$idwert,$anzds) {
  echo "<br>";	
  $dbnchopen=dbopentyp($gdbtyp,$gdbname,$gdbuser,$gdbpass);
  $dbtable=$_POST['dbtable'];
  $dbfldindex=$_POST['dbfldindex'];
  $dbcolumn=$_POST['dbcolumn'];
  $dbsyncnr=$_POST['dbsyncnr'];

  $dbtyp=$_POST['dbtyp'];
  $database=$_POST['database'];
  $dbuser=$_POST['dbuser'];
  $dbpassword=$_POST['dbpassword'];

  $remotepfad=$_POST['remotepfad'];
  $arrcolumn = explode(",", $dbcolumn);
  $arrdaten=json_decode($_POST['strdaten']);
  $colcnt=$_POST['stranzcol'];
  $arrcnt=count($arrdaten);
  $cntinh=0;
  echo "<div class='alert alert-info'>";
  for($i=1; $i <= $anzds; $i++) {
  	 $query="SELECT * FROM ".$dbtable." WHERE ".$dbfldindex."=".$arrdaten[$cntinh];
  	 echo "<br>".$query."<br>";
    $res = dbquerytyp($gdbtyp,$dbnchopen,$query);
    if ($linval = dbfetchtyp($gdbtyp,$res)) {
      $cntinh=$cntinh+1;
  	   $strupinh=$arrcolumn[1]."='".str_replace("#"," ",$arrdaten[$cntinh])."'";
  	   for($j=2; $j<$colcnt; $j++) {
  	 	  $cntinh++;
  	 	  $strupinh=$strupinh.", ".$arrcolumn[$j]."='".str_replace("#"," ",$arrdaten[$cntinh])."'";
  	   }
  	   $strupinh=$strupinh." WHERE ".$dbfldindex."=".$arrdaten[$colcnt*($i-1)];
      $cntinh++;
  	   $strupd="UPDATE ".$dbtable." SET ".$strupinh;
      echo $i.") ".$strupd."<br>";
      dbexecutetyp($gdbtyp,$dbnchopen,$strupd);
    } else {
  	   $strinh="'".$arrdaten[$cntinh]."'"; 
  	   for($j=1; $j<$colcnt; $j++) {
  	 	  $cntinh++;
  	 	  $strinh=$strinh.",'".str_replace("#"," ",$arrdaten[$cntinh])."'";
  	   }
      $cntinh++;
  	   $strins="INSERT INTO ".$dbtable." (".$dbcolumn.") VALUES (".$strinh.")";
  	   echo $i.") ".$strins."<br>";
  	   dbexecutetyp($gdbtyp,$dbnchopen,$strins);
    }
  }	  
  echo "</div>";
  $aktpfad=$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME']; 
  $aktpfad=substr($aktpfad,0,strlen($aktpfad)-8)."showtab.php"; 
  $callbackurl="http://".$aktpfad."?menu=".$menu."&idwert=".$idwert;
  $website=$remotepfad."classes/syncfertig.php?menu=".$menu."&idwert=".$idwert;
//  echo $website."<br>";
  echo "<form class='form-horizontal' method='post' action='".$website."'>";
  echo "<input type='hidden' name='callbackurl' value=".$callbackurl." />";
  echo "<input type='hidden' name='dbsyncnr' value=".$dbsyncnr." />";
  echo "<input type='hidden' name='dbtable' value=".$dbtable." />";
  echo "<input type='hidden' name='dbtyp' value=".$dbtyp." />";
  echo "<input type='hidden' name='database' value=".$database." />";
  echo "<input type='hidden' name='dbuser' value=".$dbuser." />";
  echo "<input type='hidden' name='dbpassword' value=".$dbpassword." />";
  echo "<input type='submit' value='Daten abschließen' />";
  echo "</form>";
}

?>