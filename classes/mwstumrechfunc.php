<?php
/************************
* Author: Horst Meyer
* date..: 2020-08-19
* remark: function modul for Value added tax conversion
************************/

function mwstumrech_abfrage($idwert,$preisA,$preisB,$vonmwstsatz1,$nchmwstsatz1,$vonmwstsatz2,$nchmwstsatz2) {
  echo "<form name='eingabe' class='form-horizontal' method='post' action='mwstumrech.php?mwstumrech=1&id=".$idwert."'>";
  echo "          <div class='control-group'>";
  echo "            <label class='control-label' style='text-align:left' for='input01'>Umrechnungssatz (A)</label>";
  echo "            <div class='input'>";
  echo "              <select id='input01' name='mwsttypA' size='1'>";
  echo "              <option style='background-color:#c0c0c0;' value='var1' selected>von ".$vonmwstsatz1."% zu ".$nchmwstsatz1."%</option>";
  echo "              <option style='background-color:#c0c0c0;' value='var2' >von ".$vonmwstsatz2."% zu ".$nchmwstsatz2."%</option>";
  echo "              <option style='background-color:#c0c0c0;' value='var3' >von ".$nchmwstsatz1."% zu ".$vonmwstsatz1."%</option>";
  echo "              <option style='background-color:#c0c0c0;' value='var4' >von ".$nchmwstsatz2."% zu ".$vonmwstsatz2."%</option>";
  echo "              </select>";
  echo "          </div>";
  echo "          </div>";
  echo "          <div class='control-group'>";
  echo "            <label class='control-label' style='text-align:left' for='input02'>Umrechnungssatz (B)</label>";
  echo "            <div class='input'>";
  echo "              <select id='input02' name='mwsttypB' size='1'>";
  echo "              <option style='background-color:#c0c0c0;' value='var1' >von ".$vonmwstsatz1."% zu ".$nchmwstsatz1."%</option>";
  echo "              <option style='background-color:#c0c0c0;' value='var2' selected>von ".$vonmwstsatz2."% zu ".$nchmwstsatz2."%</option>";
  echo "              <option style='background-color:#c0c0c0;' value='var3' >von ".$nchmwstsatz1."% zu ".$vonmwstsatz1."%</option>";
  echo "              <option style='background-color:#c0c0c0;' value='var4' >von ".$nchmwstsatz2."% zu ".$vonmwstsatz2."%</option>";
  echo "              </select>";
  echo "          </div>";
  echo "          </div>";
  
  echo "          <div class='control-group'>";
  echo "            <label class='control-label' style='text-align:left' for='input03'>Alter Preis (A)</label>";
  echo "            <div class='input'>";
  echo "              <input type='text' id='input03' name='preisA' value='".$preisA."'>";
  echo "            </div>";
  echo "          </div>";
  echo "          <div class='control-group'>";
  echo "            <label class='control-label' style='text-align:left' for='input04'>Alter Preis (B)</label>";
  echo "            <div class='input'>";
  echo "              <input type='text' id='input04' name='preisB' value='".$preisB."'>";
  echo "            </div>";
  echo "          </div>";

  echo "  <div class='form-actions'>";
  echo "     <button type='submit' name='submit' class='btn btn-primary'>Umrechnen</button>";
  echo "  </div>";
  
  
  echo "</form>";
}


function mwstumrechnung($idwert,$mwsttypA,$preisA,$mwsttypB,$preisB,$vonmwstsatz1,$nchmwstsatz1,$vonmwstsatz2,$nchmwstsatz2,$waehrung) {
  echo "<table>";
  if ($mwsttypA=="var1") {
    $vonmwstsatzA=$vonmwstsatz1;
    $nchmwstsatzA=$nchmwstsatz1;
  }
  if ($mwsttypA=="var2") {
    $vonmwstsatzA=$vonmwstsatz2;
    $nchmwstsatzA=$nchmwstsatz2;
  }
  if ($mwsttypA=="var3") {
    $vonmwstsatzA=$nchmwstsatz1;
    $nchmwstsatzA=$vonmwstsatz1;
  }
  if ($mwsttypA=="var4") {
    $vonmwstsatzA=$nchmwstsatz2;
    $nchmwstsatzA=$vonmwstsatz2;
  }
  if ($mwsttypB=="var1") {
    $vonmwstsatzB=$vonmwstsatz1;
    $nchmwstsatzB=$nchmwstsatz1;
  }
  if ($mwsttypB=="var2") {
    $vonmwstsatzB=$vonmwstsatz2;
    $nchmwstsatzB=$nchmwstsatz2;
  }
  if ($mwsttypB=="var3") {
    $vonmwstsatzB=$nchmwstsatz1;
    $nchmwstsatzB=$vonmwstsatz1;
  }
  if ($mwsttypB=="var4") {
    $vonmwstsatzB=$nchmwstsatz2;
    $nchmwstsatzB=$vonmwstsatz2;
  }
  $nettopreisA=round($preisA*100/(100+$vonmwstsatzA),2);
  $mwstA=round($nettopreisA*$vonmwstsatzA/100,2);
  $neuermwstA=round($nettopreisA*$nchmwstsatzA/100,2);
  $neuerpreisA=round($neuermwstA+$nettopreisA,2);
  $ersparnisA=$preisA-$neuerpreisA;	
  $nettopreisB=$preisB*100/(100+$vonmwstsatzB);
  $mwstB=$nettopreisB*$vonmwstsatzB/100;
  $neuermwstB=$nettopreisB*$nchmwstsatzB/100;
  $neuerpreisB=$neuermwstB+$nettopreisB;
  $ersparnisB=$preisB-$neuerpreisB;
  $gesamtpreis=$preisA+$preisB;
  $gesamtmwst=$mwstA+$mwstB;
  $gesamtnettopreis=$nettopreisA+$nettopreisB;
  $gesamtneuermwst=$neuermwstA+$neuermwstB;
  $gesamtneuerpreis=$neuerpreisA+$neuerpreisB;
  $gesamtersparnis=$ersparnisA+$ersparnisB;	
  if ($preisA<>0) {
    echo "<tr><td>(A) Umrechnung:</td><td>von ".$vonmwstsatzA."% nach ".$nchmwstsatzA."%</td></tr>";
    echo "<tr><td>(A) Alter Preis:</td><td style='text-align:right'>".number_format($preisA,2)." ".$waehrung."</td></tr>";
    echo "<tr><td>(A) <i>".$vonmwstsatzA."% MwSt.:</i></td><td style='text-align:right'>".number_format($mwstA,2)." ".$waehrung."</td></tr>";
    echo "<tr><td>(A) Netto-Preis:</td><td style='text-align:right'>".number_format($nettopreisA,2)." ".$waehrung."</td></tr>";
    echo "<tr><td>(A) <i>".$nchmwstsatzA."% MwSt.:</i></td><td style='text-align:right'>".number_format($neuermwstA,2)." ".$waehrung."</td></tr>";
    echo "<tr><td>(A) Neuer Preis:</td><td style='text-align:right'>".number_format($neuerpreisA,2)." ".$waehrung."</td></tr>";
    echo "<tr><td><u>(A) Ersparnis:</u></td><td style='text-align:right'><u>".number_format($ersparnisA,2)." ".$waehrung."</u></td></tr>";
  }
  if ($preisB<>0) {
    echo "<tr><td>(B) Umrechnung:</td><td>von ".$vonmwstsatzB."% nach ".$nchmwstsatzB."%</td><td></td></tr>";
    echo "<tr><td>(B) Alter Preis:</td><td style='text-align:right'>".number_format($preisB,2)." ".$waehrung."</td></tr>";
    echo "<tr><td>(B) ".$vonmwstsatzB."% MwSt.:</td><td style='text-align:right'>".number_format($mwstB,2)." ".$waehrung."</td></tr>";
    echo "<tr><td>(B) Netto-Preis:</td><td style='text-align:right'>".number_format($nettopreisB,2)." ".$waehrung."</td></tr>";
    echo "<tr><td>(B) ".$nchmwstsatzB."% MwSt.:</td><td style='text-align:right'>".number_format($neuermwstB,2)." ".$waehrung."</td></tr>";
    echo "<tr><td>(B) Neuer Preis:</td><td style='text-align:right'>".number_format($neuerpreisB,2)." ".$waehrung."</td></tr>";
    echo "<tr><td><u>(B) Ersparnis:</u></td><td style='text-align:right'><u>".number_format($ersparnisB,2)." ".$waehrung."</u></td></tr>";
  }
  if ($preisA<>0 and $preisB<>0) {
    echo "<tr><td>Gesamter alter Preis:</td><td style='text-align:right'>".number_format($gesamtpreis,2)." ".$waehrung."</td></tr>";
    echo "<tr><td><i>Gesamt-MwSt. alt:</i></td><td style='text-align:right'>".number_format($gesamtmwst,2)." ".$waehrung."</td></tr>";
    echo "<tr><td>Gesamt-Netto-Preis:</td><td style='text-align:right'>".number_format($gesamtnettopreis,2)." ".$waehrung."</td></tr>";
    echo "<tr><td><i>Gesamt-MwSt. neu:</i></td><td style='text-align:right'>".number_format($gesamtneuermwst,2)." ".$waehrung."</td></tr>";
    echo "<tr><td>Gesamter neuer Preis:</td><td style='text-align:right'>".number_format($gesamtneuerpreis,2)." ".$waehrung."</td></tr>";
    echo "<tr><td><b>Gesamtersparnis:<b></td><td style='text-align:right'><b>".number_format($gesamtersparnis,2)." ".$waehrung."</b></td></tr>";
  }
  echo "</table>";
  echo "<a href='mwstumrech.php?id=".$idwert."&preisA=".$preisA."&preisB=".$preisB."' class='btn btn-primary btn-sm active' role='button'>MwStUmrechnung</a>"; 
}

?>