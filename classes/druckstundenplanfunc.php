<?php

function druckstundenplanauswahl($gdbcon,$arrelement,$menu,$user) {

  $db = new SQLite3('../data/joorgsqlite.db');
  $qry="SELECT * FROM tblfilter WHERE fldfeld = 'fldid_user'";
  $res = $db->query($qry);
  $lin = $res->fetchArray();
  $flduserid=$lin['fldwert'];
  
  echo "<form name='eingabe' class='form-horizontal' method='post' action='druckstundenplan.php?drucken=1&menu=".$menu."&user=".$user."' enctype='multipart/form-data'>";
  echo "  <fieldset>";
      echo "          <div class='control-group'>";
      echo "            <label class='control-label' style='text-align:left' for='input01'>Benutzer</label>";

        $fquery = "SELECT * FROM tblbenutzer";
        $fresult = $db->query($fquery);
        echo "  <select name='benutzer' size='1'>";
        while ($fline = $fresult->fetchArray()) {
          $strstatus = $fline['fldbez'];
          if ($flduserid == $fline['fldindex']) {
            echo "<option style='background-color:#c0c0c0;' value=".$fline['fldindex']." selected>".$strstatus."</option>";
          } else {
            echo "<option style='background-color:#c0c0c0;' value=".$fline['fldindex']." >".$strstatus."</option>";
          }  
        }
        echo "  </select>";
	  
      echo "          </div>";
  echo "  <div class='form-actions'>";
  echo "     <button type='submit' name='submit' class='btn btn-primary'>Speichern</button>";
  echo "     <button class='btn'>Abbruch</button>";
  echo "  </div>";
  echo "  </fieldset>";
  echo "</form>";
}


function druckstundenplan($gdbcon,$benutzerid,$menu,$user) {
include ("../config.php");
echo "<html>";
echo "<link rel='stylesheet' type='text/css' href='css/style.css' />";
echo "<body>";
echo "<a href='showtab.php?menu=".$menu."&user=".$user."'  class='btn btn-primary btn-sm active' role='button'>Zurück</a><br> ";
$db = new SQLite3('../data/joorgsqlite.db');
$query="SELECT * FROM tblbenutzer WHERE fldindex=".$benutzerid;
$result = $db->query($query);
$line = $result->fetchArray();
echo "<h1>Stundenplan f&uumlr ".$line['fldbez']."</h1>";
echo "<table border='1' style='background-image:url(".$line['fldbackgroundfilename'].")'>";
echo "<tr height='50'>";
$query="SELECT * FROM tblstdwotag";
$result = $db->query($query);
$anzsp=0;
$arrSpInd = array();
while ($line = $result->fetchArray()) {
  if ($line['fldfarbe']<>"") { 
    echo "<td width='200' style='background-color:".$line['fldfarbe']."; font-size:24px; text-shadow: 1px 0 black, 0 -1px black; padding-left: 5px;'>".$line['fldbez']."</td>";
  } else {
    echo "<td width='200' style='background-color:#ff0000; font-size:24px; text-shadow: 1px 0 black, 0 -1px black; padding-left: 5px;'>".$line['fldbez']."</td>";
  }	
  $arrSpInd[] = $line[fldindex];
  $anzsp=$anzsp+1;
}  
echo "</tr>";

$query="SELECT * FROM tblstdzeit ORDER BY fldbez";
$result = $db->query($query);
while ($line = $result->fetchArray()) {
  echo "<tr height='50'>";
  echo "<td width='200' style='font-size:24px; text-shadow: 1px 0 black, 0 -1px black; padding-left: 5px;'>".$line['fldbez']."</td>";
  for ($i = 2; $i <= $anzsp; $i++) {
    $qrystd="SELECT * FROM tblstdplan WHERE fldid_user=".$benutzerid." AND fldid_wotag=".$arrSpInd[$i-1]." AND fldid_std=".$line['fldindex'];
    $resstd = $db->query($qrystd);
    if ($linstd = $resstd->fetchArray()) {
      echo "<td width='200' style='font-size:24px; text-shadow: 1px 0 black, 0 -1px black; padding-left: 5px;'>".$linstd['fldbez']."</td>";
	} else {
      echo "<td width='200' class='tabWotag'>.</td>";
	}  
  }  
  echo "</tr>";
}  
echo "</table>";
echo "</body>";
echo "</html>";
}

?>