<?php
include("bootstrapfunc.php");
include("dbtool.php");
bootstraphead('');
bootstrapbegin("Datenaustausch",'');
$callbackurl=$_POST['callbackurl'];
$remotepfad=$_POST['remotepfad'];
$dbtable=$_POST['dbtable'];
$dbfldindex=$_POST['dbfldindex'];
$dbsyncnr=$_POST['dbsyncnr'];
$dbtyp=$_POST['dbtyp'];
$database=$_POST['database'];
$dbuser=$_POST['dbuser'];
$dbpassword=$_POST['dbpassword'];
$utf8encode=$_POST['utf8encode'];
$dbcolumn=json_decode($_POST['dbcolumn']);
$debug=$_GET['debug'];
if ($debug="J") {
  echo "<div class='alert alert-info'>";
  echo "dbtyp=".$dbtyp."<br>";
  echo "dbsyncnr=".$dbsyncnr."<br>";
  echo "remotepfad=".$remotepfad."<br>";
  echo "utf8encode=".$utf8encode."<br>";
  echo "database=".$database."<br>";
  echo "table=".$dbtable."<br>";
  echo "fldindex=".$dbfldindex."<br>";
  echo "dbuser=".$dbuser."<br>";
  echo "dbpassword=".$dbpassword."<br>";
  echo "</div>";
}
echo "<a href='".$callbackurl."'  class='btn btn-primary btn-sm active' role='button'>Zurück</a> ";

echo "<form class='form-horizontal' method='post' action='".$callbackurl."'>";
echo "<input type='hidden' name='status' value='einspielen' />";
echo "<br>";

//$query="SELECT * FROM tblsyncstatus WHERE fldtable='".$dbtable."' AND flddbsyncnr=".$arrdbnchsyncnr[$tablecount];

$dbnchopen=dbopentyp($dbtyp,$database,$dbuser,$dbpassword);
$column=getdbcolumn($dbtyp,$database,$dbtable,$dbuser,$dbpassword);
if ($dbcolumn==$column) {
  //mysqli_query($dbnchopen,"SET NAMES 'utf8'");
  $query="SELECT * FROM tblsyncstatus WHERE fldtable='".$dbtable."' AND flddbsyncnr=".$dbsyncnr;
  //echo $query."<br>";
  $resst = dbquerytyp($dbtyp,$dbnchopen,$query);
  if ($linst = dbfetchtyp($dbtyp,$resst)) {
    $timestamp=$linst['fldtimestamp'];
  } else {
    $timestamp='2015-01-01 00:00:00'; 
  }
  $qryval = "SELECT * FROM ".$dbtable." WHERE flddbsyncstatus='SYNC' AND fldtimestamp>'".$timestamp."'";
  echo "<div class='alert alert-info'>";
  echo $qryval."<br>";
  echo "</div>";
  $resval = dbquerytyp($dbtyp,$dbnchopen,$qryval);
  echo "<br>";
  echo "<table class='table table-hover'>";
  echo "<tr><th>Index</th><th>Bezeichnung</th><th>inh</th></tr>";
  $datcnt=0;
  $arrdaten = array();
  while ($linval = dbfetchtyp($dbtyp,$resval)) {
    $datcnt=$datcnt+1;
    $arrcolumn = explode(",", $dbcolumn);
    for($colcnt = 0; $colcnt < count($arrcolumn); $colcnt++) {
      $inh=$linval[$arrcolumn[$colcnt]];
		$inh=str_replace(" ","#",$inh);
		$inh=str_replace("'","",$inh);
		$inh=utf8_encode($inh);
      array_push($arrdaten,$inh);
    }
    echo "<tr>";
    echo "<td>".$linval['fldIndex']."</td>";
    if ($utf8encode="J") {
      echo "<td>".utf8_encode($linval['fldBez'])."</td>";
    } else {
      echo "<td>".$linval['fldBez']."</td>";
    }
    echo "<td>".count($arrdaten)."</td>";
    echo "</tr>";
  }
  echo "</table>";
  echo "<input type='hidden' name='stranzds' value=".$datcnt." />";
  echo "<input type='hidden' name='stranzcol' value=".$colcnt." />";
  echo "<input type='hidden' name='dbtable' value=".$dbtable." />";
  echo "<input type='hidden' name='dbfldindex' value=".$dbfldindex." />";
  echo "<input type='hidden' name='dbcolumn' value=".$column." />";
  echo "<input type='hidden' name='dbsyncnr' value=".$dbsyncnr." />";

  echo "<input type='hidden' name='database' value=".$database." />";
  echo "<input type='hidden' name='dbtyp' value=".$dbtyp." />";
  echo "<input type='hidden' name='dbuser' value=".$dbuser." />";
  echo "<input type='hidden' name='dbpassword' value=".$dbpassword." />";

  echo "<input type='hidden' name='remotepfad' value=".$remotepfad." />";
  echo "<input type='hidden' name='strdaten' value=".json_encode($arrdaten)." />";
  echo "<input type='submit' value='Daten einspielen' />";
} else {
  echo "<div class='alert alert-info'>";
  echo "von :".$dbcolumn."<br>";
  echo "nach:".$column."<br>";
  echo $dbtable." no ok.";
  echo "</div>";
}

echo "</form>";
bootstrapend();

?>