<?php

function druckdauer_abfrage($idwert,$dauer) {
  echo "<form name='eingabe' class='form-horizontal' method='post' action='3ddruckdauer.php?druckdauer=1&id=".$idwert."'>";
  echo "          <div class='control-group'>";
  echo "            <label class='control-label' style='text-align:left' for='input03'>aktuelle Dauer (Std.)</label>";
  echo "            <div class='input'>";
  echo "              <input type='text' id='input03' name='aktdauerstd' value=''>";
  echo "            </div>";
  echo "          </div>";
  echo "          <div class='control-group'>";
  echo "            <label class='control-label' style='text-align:left' for='input02'>aktuelle Dauer (min.)</label>";
  echo "            <div class='input'>";
  echo "              <input type='text' id='input02' name='aktdauermin' value=''>";
  echo "            </div>";
  echo "          </div>";
  echo "          <div class='control-group'>";
  echo "            <label class='control-label' style='text-align:left' for='input01'>aktueller Prozentsatz</label>";
  echo "            <div class='input'>";
  echo "              <input type='text' id='input01' name='aktproz' value=''>";
  echo "            </div>";
  echo "          </div>";
  echo "<input type='hidden' name='dauer' value='".$dauer."'>";

  echo "  <div class='form-actions'>";
  echo "     <button type='submit' name='submit' class='btn btn-primary'>Umrechnen</button>";
  echo "  </div>";
  
  
  echo "</form>";
}

function druckdauer_berechnen($idwert,$aktdauerstd,$aktdauermin,$aktproz,$dauer) {
 $stddauer="00";
 $aktdaueranz=$aktdauermin;
 if ($aktdauerstd>0) {
 	$aktdauermin=$aktdauermin+$aktdauerstd*60;
 }	
 $mindauer=$aktdauermin / $aktproz * 100;
 if ($mindauer>60) {
 	$stddauer=$mindauer/60;
 	$stddauer=intval($stddauer);
 	$stdlen=strlen($stddauer);
 	if ($stdlen==1) {
 	  $stddauer="0".$stddauer;	 
 	} 
 	$mindauer=$mindauer-$stddauer*60;
 }
 $mindauer=number_format($mindauer,0);
 $minlen=strlen($mindauer);
 if ($minlen==1) {
 	$mindauer="0".$mindauer;
 }
 $stdrest=$stddauer;
 if ($mindauer-$aktdaueranz>=0) {
   $minrest=$mindauer-$aktdaueranz;
   $stdrest=$stdrest-$aktdauerstd;
 } else {
 	$minrest=$aktdaueranz-$mindauer;
 	$minrest=60-$minrest;
 	$stdrest=$stdrest-1;
 	$stdrest=$stdrest-$aktdauerstd;
 }  
 $vrstldruckdauer=$stddauer.":".$mindauer;
 $minlen=strlen($minrest);
 if ($minlen==1) {
 	$minrest="0".$minrest;
 }
 $minlen=strlen($stdrest);
 if ($minlen==1) {
 	$stdrest="0".$stdrest;
 }
 $restdauer=$stdrest.":".$minrest;	
 $stduhrzeit=date("H");
 $minuhrzeit=date("i");
 $stduhrzeit=$stduhrzeit+$stdrest;
 $minuhrzeit=$minuhrzeit+$minrest;
 if ($minuhrzeit>59) {
 	$minuhrzeit=$minuhrzeit-60;
 	$stduhrzeit=$stduhrzeit+1; 
 }
 $tage="";
 if ($stduhrzeit>23) {
 	$tage=intval($stduhrzeit/24);
 	$stduhrzeit=$stduhrzeit-$tage*24;
 	$tage=" (+".$tage." Tage)";
 }
 $minlen=strlen($minuhrzeit);
 if ($minlen==1) {
 	$minuhrzeit="0".$minuhrzeit;
 }
 $uhrzeit=$stduhrzeit.":".$minuhrzeit.$tage;
 echo "<table>";
 echo "<tr><td>akt. Dauer (std.):</td><td style='text-align:right'>".$aktdauerstd."</td><td></td></tr>";
 echo "<tr><td>akt. Dauer (min.):</td><td style='text-align:right'>".$aktdaueranz."</td><td></td></tr>";
 echo "<tr><td>akt. Prozentsatz:</i></td><td style='text-align:right'>".$aktproz."%</td><td></td></tr>";
 if ($dauer<>"") {
   echo "<tr><td><u>Vorausstl. Druckdauer : </u></td><td style='text-align:right'> <u>".$vrstldruckdauer."</u></td><td> (".$dauer.")</td></tr>";
 } else {
   echo "<tr><td><u>Vorausstl. Druckdauer : </u></td><td style='text-align:right'> <u>".$vrstldruckdauer."</u></td><td></td></tr>";
 }
 echo "<tr><td><u>Restdauer : </u></td><td style='text-align:right'> <u>".$restdauer."</u></td><td></td></tr>";
 echo "<tr><td>fertig um : </td><td style='text-align:right'> ".$uhrzeit."</td><td></td></tr>";
 echo "</table><br>";
 echo "<a href='3ddruckdauer.php?id=".$idwert."&dauer=".$vrstldruckdauer."' class='btn btn-primary btn-sm active' role='button'>3D-Druckdauer</a>"; 
}
 
?>