<?php
session_start();

function vorratauswahl($menu) {
  echo "<form class='form-horizontal' method='post' action='vorrat.php?vorrat=1&menu=".$menu."'>";
  echo " <input type='submit' value='Speichern' />";
  echo "</form>";
}

function vorratfunc($menu,$pararray) {
  $dbselarr = $_SESSION['DBSELARR'];
  $count=sizeof($dbselarr);
  //echo $count."=count<br>";
  if ($count==0) {
    echo "<div class='alert alert-warning'>";
    echo "keinen Datensatz gefunden.";
    echo "</div>";
  } else {
    $db = new SQLite3('../data/joorgsqlite.db');
    echo "<br>";
    for($zaehl = 0; $zaehl < $count; $zaehl++) {
      $qry="SELECT * FROM tblEinkauf_liste WHERE ".$pararray['fldindex']."=".$dbselarr[$zaehl];
      $res = $db->query($qry);
      if ($row = $res->fetchArray() ) { 
        $nr=$zaehl+1;
        if ($row['fldBarcode']<>"") {
          $qrystamm="SELECT * FROM tblartikel WHERE fldbarcode='".$row['fldBarcode']."'";
          $resstamm = $db->query($qrystamm);
          if ($rowstamm = $resstamm->fetchArray() ) {
            $gef="aktualisiert";	 
           	$sql="UPDATE tblartikel SET fldPreis='".$row['fldPreis']." WHERE fldBarcode='".$row['fldBarcode']."'";
          } else {
            $gef="eingefügt";	
            $sql="INSERT INTO tblartikel (fldBez,fldTyp,fldAbteilung,fldOrt,fldPreis,fldKonto,fldAnz,fldbarcode,fldverpackbez,fldverpackmeng,fldJN) VALUES ('".$row['fldBez']."','".$row['fldTyp']."','','".$row['fldOrt']."','".$row['fldPreis']."','".$row['fldKonto']."','1','".$row['fldBarcode']."','','','N')";
          }
          $qryexe = $db->exec($sql);
          // Vorrat aktualisieren
          $timestamp="datetime('now', 'localtime')";
          $qryvorrat="SELECT * FROM tblvorrat WHERE fldbarcode='".$row['fldBarcode']."' and fldortkurz='".$row['fldortkurz']."'";
          $resvorrat = $db->query($qryvorrat);
          if ($rowvorrat = $resvorrat->fetchArray() ) {
         	$sqlvorrat="UPDATE tblvorrat SET fldanz=fldanz+".$row['fldAnz'].", fldortkurz='".$row['fldortkurz']."', fldtimestamp=".$timestamp." WHERE fldbarcode='".$row['fldBarcode']."' and fldortkurz='".$row['fldortkurz']."'";
          } else {
         	$sqlvorrat="INSERT INTO tblvorrat (fldbarcode,fldanz,fldortkurz,fldtimestamp) VALUES('".$row['fldBarcode']."',".$row['fldAnz'].",'".$row['fldortkurz']."',".$timestamp.")";
          }	
          $qryexe = $db->exec($sqlvorrat);
          echo "<div class='alert alert-success'>";
          echo $qrystamm."<br>";
          echo $sql."<br>";
          echo $qryvorrat."<br>";
          echo $sqlvorrat."<br>";
	       echo $nr.".) ".$dbselarr[$zaehl].",".$row['fldAnz'].",".$row['fldBez'].",".$row['fldBarcode'].",".$row['fldPreis']." ".$gef."<br>";
          echo "</div>";
        }
      } else {
        echo "<div class='alert alert-warning'>";
  	     echo $dbselarr[$zaehl]." nicht gefunden<br>";
        echo "</div>";
      }
    }
  }  
}

?>